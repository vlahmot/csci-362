/*
  Filename   : Sorting.cc
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Program 1: Sorting (Sorts and Complexity Analysis)
  Description: A program capable of sorting a vector using a selection sort,
  insertion sort, partition sort and std::sort.
*/   

/************************************************************/
// System includes

#include <iostream>
#include <string>
#include <cstdlib>
#include <limits>
#include <ctime>
#include <queue>
#include <functional>
/************************************************************/
// Local includes
#include "Sorting.h"

/************************************************************/
// Using declarations

using std::cout;
using std::endl;
using std::swap;
using std::numeric_limits;
using std::stoul;
using std::time;
using std::priority_queue;
using std::boolalpha;
using std::function;
/************************************************************/
// Function prototypes/global vars/typedefs

/************************************************************/

int      
main (int argc, char* argv[]) 
{ 
  
  srand(static_cast<unsigned int>(time(0)));
  if(argc !=3)
  {
    std::cerr<< "Usage: " << argv[0] << " SORT_TYPE"<< " VECTOR_SIZE"<<endl; 
    return EXIT_FAILURE;
  }

  if(argv[1] == string("selection"))
  { 
    vector<int> v;
    for(unsigned long l = 0; l < stoul(argv[2]);l++)
    {
      v.push_back(rand() % 100 +1);
    }
    selectionSort(v);
    cout << boolalpha << isSorted(v) << endl;
  }

  if(argv[1] == string("priority"))
  {
    vector<int> v;
    for(unsigned long l = 0; l < stoul(argv[2]);l++)
    {
      v.push_back(rand() % 100 +1);
    }
    
    prioritySort(v);
    cout << boolalpha << isSorted(v) << endl;
  }

  if(argv[1] == string("insertion"))
  {
    vector<int> v;
    for(unsigned long l = 0; l < stoul(argv[2]);l++)
    {
      v.push_back(rand() % 100 +1);
    }
    
    insertionSort(v);
    cout << boolalpha << isSorted(v) << endl;
  }
  

  if(argv[1] == string("shell"))
  {
    vector<int> v;
    for(unsigned long l = 0; l < stoul(argv[2]);l++)
    {
      v.push_back(rand() % 100 +1);
    }
    
    shellSort(v);
    cout << boolalpha << isSorted(v) << endl;
  }

  if(argv[1] == string("bubble"))
  {
    vector<int> v;
    for(unsigned long l = 0; l < stoul(argv[2]);l++)
    {
      v.push_back(rand() % 100 +1);
    }
    
    bubbleSort(v);
    cout << boolalpha << isSorted(v) << endl;
  }

  return EXIT_SUCCESS; 
}

void 
shellSort (vector<int>& v)
{
  size_t h;
  size_t N = v.size();
  for(h = 1; h <=N/4;h = h*2 +1)
  {}
  while (h > 0)
  {
      
    for(size_t i = h; i < v.size();++i)
    {
      int currEle = v[i];
      for (size_t j = i; j > h ;j = j - h)
      {
        if(v[j] > currEle)
        {
          v[j+h] = v[j];  
        }
        else
        {
          v[j] = currEle; 
        }


      }
    }

  h = h/2;
  }

  
}


void
bubbleSort (vector<int>& v)
{
  bool swapped = true;
  while(swapped == true)
  {
    swapped = false;
    for(size_t i = 0; i < v.size(); ++i)
    {
      if(v[i] > v[i + 1])
      {
        swap(v[i],v[i+1]);
        swapped = true; 
      }
    }
  }
}


template <typename T>
void
selectionSort (vector<T>& v)
{
  for(size_t i = 0; i < v.size();++i)
  {
    size_t candidate = i;
    for(size_t j = i + 1; j <v.size(); ++j)
    {
      if(v[j] < v[candidate])
      {
        candidate = j;
      }
    }
    swap(v[i], v[candidate]);
  }
}

void
insertionSort (vector<int>& v) 
{
  for(size_t i = 1; i < v.size();++i)
  {
    int currEle = v[i];
    for (size_t j = i-1; j > 0; --j)
    {
      if(v[j] > currEle)
      {
        v[j+1] = v[j];  
      }
      else
      {
        v[j] = currEle; 
      }
    }
  }
}

void
prioritySort (vector<int>& v)
{
   priority_queue<int> numberQueue;
   for(size_t i = 0; i < v.size();++i)
    {
        numberQueue.push(v[i]);
    }

    for(size_t j = 0; j < v.size(); ++j)
    {
        v[v.size() - j - 1] = numberQueue.top();
        numberQueue.pop();
    }
}



bool 
isSorted (vector<int>& v)
{
  for(size_t i = 0; i < v.size() - 1;++i)
  {
    if(v[i] > v[i+1])
    {
        return false;
    }
  }
  return true;
}
/************************************************************/
/************************************************************/

