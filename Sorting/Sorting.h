/*
  Filename   : Sorting.h
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Program 1: Sorting (Sorts and Complexity Analysis)
  Description: A program capable of sorting a vector using a selection sort, insertion sort, and std::sort.
*/   

/************************************************************/
// Macro guard to prevent multiple inclusions

#ifndef SORTING_H         
#define SORTING_H

/************************************************************/
// System includes

#include <string>
#include <iostream>
#include <vector>
#include <functional>
/************************************************************/
// Local includes
/************************************************************/
// Using declarations

using std::string;
using std::ostream;
using std::vector;
using std::function;
/************************************************************/
//Enum declaration
enum SortType {Selection,Insertion,Tree,PriorityQueue,Bubble,
               Merge,Quick,Heap,Radix,Bucket
              };

/************************************************************/
/************************************************************/
template <typename T>
void selectionSort (vector<T>& v);

void insertionSort (vector<int>& v);

void prioritySort (vector<int>& v);

void bubbleSort (vector<int>& v);

void shellSort (vector<int>& v);

bool isSorted (vector<int>& v);





/************************************************************/

#endif

/************************************************************/

