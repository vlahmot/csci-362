/*
  Filename   : Lambdas.cpp
  Author     : Tom Hlavaty 
  Course     : CSCI 362-01
  Description: Lamda Expressions. 
*/   

/************************************************************/
// System includes

#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
/************************************************************/
// Local includes


/************************************************************/
// Using declarations
using std::cout;
using std::endl;
using std::function;
using std::vector;
using std::for_each;
/************************************************************/
int main()
{
  // form of lambda
  // [] () mutable throw() -> int {}
  // [] lambda introducer
  // () lambda parameter declaration list
  // mutable mutable-specification
  // throw() exception-specification
  // -> <type> lambda reuturn type clause 
  //{} compound statement (body)
  //(5) is a call to the lambda
  

  auto identity = [] (int i) {return i;};
  auto square = [&]  mutable (int x) {x*=x;};
  cout << "Square of 5 is " << square(5) << endl;

  vector<int> v = {1,2,3,4,5,6,7,8,9,10};
  for_each(v.begin(),v.end(),square);
  for(int i : v)
  {
    cout << i << endl;
  }
  auto y = 6;
  cout << identity(5) + y << endl;

  int m = 
  [](int x) {return [] (int y) {return y * 2;} (x) + 3 ;}(5);  
  //The first lambda expression returns a second lambda that multplies
  //an int by 2. The inner lambda is called with (x) and then 3 is added.
  //The outer lambda is called with 5.
  
  cout << m << endl;

  auto j = [](int x) -> function<int (int)> 
  {return [=] (int y) {return x * y; };};

  int r = j(5)(5);
  function<int (int)> g = j(6);

  cout << r << endl;
  cout << g(6) << endl;
}
/************************************************************/
// Function prototypes/global vars/typedefs

/************************************************************/

/************************************************************/

