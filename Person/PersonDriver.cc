/*
  Filename   : PersonDriver.cc
  Author     : Tom Hlavaty 
  Course     : CSCI 362-01
  Assignment : Create a class to represent a person. The class should hold the
               name and birthdate of the person. 
  Description: Generic structure of a C++ source file
*/   

/************************************************************/
// System includes

#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>

/************************************************************/
// Local includes

# include "Person.h"

/************************************************************/
// Using declarations

using std::cout;
using std::endl;
using std::vector;
/************************************************************/
// Function prototypes/global vars/typedefs

/************************************************************/

int      
main (int argc, char* argv[]) 
{        
  Person p1, p2(1980,"Bob"), p3(1970, "Steve"), p4(0,"jesus");
  Person personArray[] = {p1,p2,p3,p4};
  
  vector<Person> people (personArray, personArray + 4);
  for(size_t i= 0; i < people.size(); ++i)
  {
  	cout<< people[i] <<endl;
  }

  for(size_t i=0; i < people.size(); ++i)
  {
    people[i].makeOlder (100);
    cout<< people[i] <<endl;

  }

  cout<< endl;
  
  return EXIT_SUCCESS; 
}

/************************************************************/
