/*
  Filename   : Template.cc
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Create a class for representing a Person.
*/   

/************************************************************/
// System includes

#include <iostream>
#include <string>
#include <cstdlib>

/************************************************************/
// Local includes

#include "Person.h"

/************************************************************/
// Using declarations

using std::cout;
using std::endl;

/************************************************************/
// Function prototypes/global vars/typedefs

Person::Person (unsigned int birthDate, string name)
	: m_birthDate (birthDate), m_name (name)

{
}


/************************************************************/
void
Person:: makeOlder(unsigned int amount)
{
	m_birthDate -= amount;
}

/************************************************************/
unsigned int
Person::getAge() const
{
	return 2013- m_birthDate;
}
/************************************************************/
bool
Person::isAdult() const
{
	return getAge() >= 18;
}

/************************************************************/
void
Person::output(ostream& outStream) const
{
	outStream << "Name is " << m_name <<": ";
	outStream << "Age is " << getAge(); 
	outStream << endl;
} 

/************************************************************/
//Free functions associated with class.

ostream&
operator<< (ostream& outStream, const Person& p)
{
	p.output(outStream);
	return outStream;
}
/************************************************************/

