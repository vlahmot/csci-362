/*
  Filename   : Person.cc
  Author     : Tom Hlavaty 
  Course     : CSCI 362-01
  Assignment : Create a class for representing a person.
  Description: Holds the class declaration details for the person class. 
*/   

/************************************************************/
// Macro guard to prevent multiple inclusions

#ifndef PERSON_H
#define PERSON_H
/************************************************************/
// System includes

#include <string>
#include <iostream>

/************************************************************/
// Local includes

/************************************************************/
// Using declarations

using std::string;
using std::ostream;

/************************************************************/

class Person
{
public:
	Person (unsigned int birthDate = 0, string name = "");

	void makeOlder (unsigned int amount);
	unsigned int getAge() const; 
	bool isAdult() const;
	void output(ostream& outStream) const;	
  
private:

	int m_birthDate;
	string m_name;

};

//Free Functions

ostream&
operator<< (ostream& outStream, const Person& p);


#endif

/************************************************************/

