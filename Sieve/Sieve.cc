/*
  Filename   : Sieve.cc
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Implement Sieve of Eratosthenes
*/   

/************************************************************/
// System includes

#include <iostream>
#include <string>
#include <cstdlib>
#include <set>
#include <math.h>	
#include <time.h>
#include <ctime>
#include <iomanip>
/************************************************************/
// Local includes


/************************************************************/
// Using declarations

using std::cout;
using std::endl;
using std::cin;
using std::set;
using std::setprecision;
using std::fixed;

/************************************************************/
// Function prototypes/global vars/typedefs
set<int> sieve (int n);
void markOff (set<int>&, set<int>::iterator iter, int& i);

/************************************************************/

int      
main (int argc, char* argv[]) 
{
  cout << "Enter the upper bound: " << endl;    
  int upper;
  cin >> upper;
  
  timespec startTime;
  timespec endTime;

  
  clock_gettime(CLOCK_MONOTONIC,&startTime);
  set<int> primes = sieve(upper);
  clock_gettime(CLOCK_MONOTONIC,&endTime);
  
   
  /*
  for(auto i = primes.begin(); i != primes.end(); ++i)
  {
    cout << (*i) << endl;
  }
  */
  
  //cout << "Seconds: " << endTime.tv_sec - startTime.tv_sec << endl;
  //cout << "NanoSeconds: " << endTime.tv_nsec - startTime.tv_nsec <<endl;	
  cout << "Miliseconds: " << fixed << setprecision(2) << ((endTime.tv_sec - startTime.tv_sec) * 1000) + static_cast<double>((endTime.tv_nsec - startTime.tv_nsec)) / 1000000.0 << endl;
  
  
  
  return EXIT_SUCCESS; 
}

set<int> sieve (int n)
{
  set<int> intSet;
  for(int i = 2; i <= n ; ++i)
  {
    intSet.insert(i);
  }
  
  int begin = 2;
  while( begin <= floor(sqrt(n)))
  {
    markOff(intSet,intSet.find(begin),begin);
    ++begin;
  }
  
  return intSet;
}
void markOff (set<int>& intSet, set<int>::iterator it, int& mult)
{
 
  for(set<int>::iterator i = it; i != intSet.end(); i+=mult)
  {
    intSet.erase(i);
  }
  
}

/************************************************************/
/************************************************************/

