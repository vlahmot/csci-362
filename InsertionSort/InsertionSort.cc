/*
  Filename   : InsertionSort.h
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Description: Implement Insertion Sort
*/   

/************************************************************/
// System includes
#include <iostream>
#include <cstdlib>
#include <vector>
/************************************************************/
// Local includes

#include "InsertionSort.h"

/************************************************************/
// Using declarations


using std::vector;
using std::cout;
using std::endl;
/************************************************************/
// Function prototypes/global vars/typedefs

/************************************************************/
int
main (int argc, char* argv[]) 
{        
    vector<int> testVec = {9,8,7,6,5,4,3,2,1};
    for(size_t x = 0; x < testVec.size(); ++x)
    {
        cout << testVec[x] << " " << endl;
    }

    insertionSort(testVec);

    
    for(size_t x = 0; x < testVec.size(); ++x)
    {
        cout << testVec[x] << " " << endl;
    }

    return EXIT_SUCCESS; 
}

void
insertionSort(vector<int>& A)
{

    size_t j = 0;
    for(size_t i = 1; i < A.size(); ++i)
    {
        int key = A[i];
        j = i -1;
        while((j>=0) && (A[j] > key))
        {
            A[j+1] = A[j];
        }
        A[j+1] = key;
    }
}
/************************************************************/
/************************************************************/

