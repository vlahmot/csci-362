/*
  Filename   : InsertionSort.h
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Description: Implement Insertion Sort
*/   

/************************************************************/
// Macro guard to prevent multiple inclusions

#ifndef InsertionSort_H         
#define InsertionSort_H

/************************************************************/
// System includes

#include <string>
#include <iostream>
#include <vector>

/************************************************************/
// Local includes

/************************************************************/
// Using declarations

using std::vector;
/************************************************************/

// Place all free function prototypes after the class declaration
//   Include only functions that act on the class

void
insertionSort(vector<int>& A);
/************************************************************/

#endif

/************************************************************/

