/*
  Filename   : TreeSort.cc
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Use TreeSort to sort the system dictionary. 
*/   

/************************************************************/
// System includes

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <algorithm>
#include <sys/time.h>
#include <queue>

/************************************************************/
// Local includes


/************************************************************/
// Using declarations
using std::vector;
using std::ifstream;
using std::multiset;
using std::is_sorted;
using std::cout;
using std::endl;
using std::boolalpha;
using std::string;
using std::copy;
using std::cerr;
using std::priority_queue;
using std::sort;
/************************************************************/
// Function prototypes/global vars/typedefs

template <typename T> void treeSort(vector<T>& v);
template <typename T> void prioritySort(vector<T>& v);
template <typename T> void shellSort(vector<T>& v);

void readText(vector<string>& words);


/************************************************************/

int
main ()
{
    vector<string> dictionaryVector;
    readText(dictionaryVector);
		vector<int> test = {1,2,3,4,5};
	
    //treeSort(dictionaryVector);
		//prioritySort(dictionaryVector);
	  //sort(dictionaryVector.begin(),dictionaryVector.end());
		shellSort(test);
		


    cout << boolalpha << is_sorted(dictionaryVector.begin(),dictionaryVector.end()) << endl;
}

void
readText(vector<string>& words)
{
    ifstream fileStream ("/home/faculty/zoppetti/Words.txt");
    if(! fileStream)
    {
        cerr << "Failed to open a file! Exiting. " << endl;
    }

    string word;
    while (fileStream >> word)
    {
        words.push_back (word);
    }
}

template <typename T>
void
treeSort (vector<T>& v)
{
    multiset<T> m(v.begin(),v.end());
    copy(m.begin(),m.end(),v.begin());
}

template <typename T> 
void
prioritySort (vector<T>& v)
{
   priority_queue<T> numberQueue;
   for(size_t i = 0; i < v.size();++i)
    {
        numberQueue.push(v[i]);
    }

    for(size_t j = 0; j < v.size(); ++j)
    {
        v[v.size() - j - 1] = numberQueue.top();
        numberQueue.pop();
    }
}

template <typename T>
void
shellSort (vector<T>& v)
{
  
  size_t d = v.size();

  while(d > 1)
  {
    d = (d + 1) / 2 ;

    for(size_t i = d; i < v.size();i+=d)
    {
      T eleToPlace = v[i];

      for(size_t j = i; j >= d; j-=d)
      {
        if(v[j-d] > eleToPlace)
        {
           v[j] = v[j-d]; 
        }
        else
        {
        v[j] = eleToPlace;
        }
      }
    }
  }


  
}






/************************************************************/


/************************************************************/

/************************************************************/
/************************************************************/

