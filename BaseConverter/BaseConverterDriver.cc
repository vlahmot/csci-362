/*
  Filename   : BaseConverterDriver.cc
  Author     : Tom Hlavaty 
  Course     : CSCI 362-01
  Description: Convert base 10 ints (positive) to base in[2,26] 
*/   

/************************************************************/
// System includes

#include <iostream>
#include <string>
#include <cstdlib>

/************************************************************/
// Local includes
#include "Stack.hpp"

/************************************************************/
// Using declarations

using std::cout;
using std::cin;
using std::endl;
using std::string;

/************************************************************/

int      
main (int argc, char* argv[]) 
{
	int numToConvert;
	int base;

	cout << "Number => ";
	cin >> numToConvert;

	cout << "Base => ";
	cin >> base;
  

  if (base < 2 || base > 16 || numToConvert < 0)
  {
    std::cerr << "Usage: " << "Number must be > 0 and Base must be in [2,16]." << endl;
    return EXIT_FAILURE;
  }
  string answer; 
  string digits = "0123456789ABCDEF";

  Stack<char> numeralStack;

  while (numToConvert > 0)
  {
    numeralStack.push(digits.at (numToConvert % base));
    numToConvert /= base;
  }

  cout << "Converted Number: " << numeralStack << endl;

  numeralStack.flip();

  cout << "Stack flipped: " << numeralStack << endl;

  return EXIT_SUCCESS; 
}

/************************************************************/
