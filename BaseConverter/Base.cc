/*
  Filename   : Base.cc
  Author     : Tom Hlavaty 
  Course     : CSCI 362-01
  Description: Convert base 10 ints(positive) to base in[2,26] 
*/   

/************************************************************/
// System includes

#include <iostream>
#include <string>
#include <cstdlib>
#include <stack>

/************************************************************/
// Using declarations

using std::cout;
using std::string;
using std::stack;

/************************************************************/

string
baseConverter ( int original, int base ) ;

/************************************************************/

int      
main ( int argc, char* argv[] ) 
{        
  cout << baseConverter ( atoi ( argv[1] ),atoi ( argv[2] ) );
  return EXIT_SUCCESS; 
}

string 
baseConverter ( int original, int base )
{
  string answer;
  string digits = "0123456789ABCDEF";

  stack<int> numeralStack;

  while ( original > 0 )
  {
    numeralStack.push(original % base);
    original /=base;
  }
  
  while ( ! numeralStack.empty() )
  {
    answer+= digits.at(numeralStack.top());
    numeralStack.pop();
  }

  return answer;
}

/************************************************************/
/************************************************************/

