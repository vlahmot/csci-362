/*
  Filename   : Stack.hpp
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Implement a Templated Stack.
  Description: Templated stack implementation. With all of the 
               basic stack methods as well as a flip method.
*/   
/************************************************************/

/************************************************************/
//Macro Guard
#ifndef STACK_HPP
#define STACK_HPP
/************************************************************/
// System includes
#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iterator> 

/************************************************************/
// Using declarations

using std::cout;
using std::vector;
using std::endl;
using std::reverse;
using std::reverse_copy;
using std::ostream;
using std::ostream_iterator;
/************************************************************/

template<typename T>
class Stack 
{
	vector<T> v;

	public:
	
		Stack () : v () {}
		
		T& top () {return v.back();}
		
		const T& top () const {return v.back();}
		
		void push (const T& i) {v.push_back(i);}

		void pop () { v.pop_back();}
		
		bool empty () const {return (v.size() == 0);}

		size_t size () const {return v.size(); }

		void flip () {reverse(v.begin(),v.end());}
		
		void display (ostream& output) 
		{ 
			ostream_iterator<char> outIter (output, " ");
			output << "[ ";
			reverse_copy(v.begin(),v.end(),outIter);
			output << "]";
		} 
};

/************************************************************/
// Free functions associated with class
template <typename T>
ostream&
operator<< (ostream& output, Stack<T>& myStack) 
{
	myStack.display(output);
	return output;
}
/************************************************************/
#endif
