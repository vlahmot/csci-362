/*
  Filename   : SelectionSort.h
  Author     : Tom Hlavaty	
  Course     : CSCI 362-01
  Assignment : Implement Selection Sort 
  Description: SelectionSort(Pick smallest and move it to the fist pos, do again
  for the second, etc. 
*/   

/************************************************************/
// Macro guard to prevent multiple inclusions

#ifndef SelectionSort_H         
#define SelectionSort_H

/************************************************************/
// System includes

#include <string>
#include <iostream>
#include <vector>
/************************************************************/
// Local includes

/************************************************************/
// Using declarations

using std::string;
using std::ostream;
using std::vector;
// Place all free function prototypes after the class declaration
//   Include only functions that act on the class

/************************************************************/
void selectionSort(vector<int>& A);
#endif

/************************************************************/

