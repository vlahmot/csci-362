/*
  Filename   : Template.cc
  Author     : Tom Hlavaty 
  Course     : CSCI 362-01
  Assignment : 
  Description: Implement Selection Sort.
*/   

/************************************************************/
// System includes

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
/************************************************************/
// Local includes

#include "SelectionSort.h"
/************************************************************/
// Using declarations

using std::cout;
using std::vector;
using std::swap;
/************************************************************/
// Function prototypes/global vars/typedefs

/************************************************************/

int      
main (int argc, char* argv[]) 
{        
    vector<int> testVector = {9,8,7,6,5,4,3,2,1};
    selectionSort(testVector);
    for(size_t i = 0; i < testVector.size(); ++i)
    {
        cout << testVector[i];
    }
  return EXIT_SUCCESS; 
}
void
selectionSort(vector<int>& A)
{
    for(size_t i = 0; i < A.size(); ++i)
    {
        for(size_t k = i+1; k < A.size(); ++k)
        {
            if(A[k]<A[i])
            {
                swap(A[k],A[i]);     
            }
        }
    }  
}
/************************************************************/
/************************************************************/

