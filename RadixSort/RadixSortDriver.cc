/*Filename   : RadixSortDriver.cc
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Implement radix sort and a Timer class.
  Description: A driver file for timing radix sort.
*/   
/************************************************************/
// System includes
#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <queue>
#include <iomanip>
/************************************************************/
// Local includes
#include "Timer.h"
/************************************************************/
// Using declarations
using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::is_sorted;
using std::boolalpha;
using std::queue;
using std::generate;
using std::setprecision;
using std::fixed;
/************************************************************/
// Function prototypes/global vars/typedefs
void radixSort (vector<int>& v);
/************************************************************/
int      
main (int argc, char* argv[]) 
{ 
  cout << "Number of integers (N) ==>";

  vector<int> randInts ( [] () {int x; cin >> x; return x;}());
  generate(randInts.begin (),randInts.end (),[] () {return rand () % 10000 + 1;}); 

  Timer timer;

  timer.start ();
  radixSort (randInts);
  timer.stop ();

  cout << boolalpha << "Sorted?" << std::is_sorted (randInts.begin (), randInts.end ()) << endl;
  cout << "Time: " << fixed << setprecision (2) << timer.getElapsedMs () << " Ms" << endl;

  return EXIT_SUCCESS; 
}

void
radixSort (vector<int>& v)
{
  vector<queue<int>> queueVector (10);

  for (size_t digitCount = 0 ; digitCount < 5 ; ++digitCount)
  {   
    for(int i : v)
    {
      queueVector[i/static_cast<int>(pow(10,digitCount)) % 10 ].push(i); 
    }
    
    size_t currPos = 0;
    for (size_t i = 0; i < queueVector.size (); ++i)
    {
      while (!queueVector[i].empty ())  
      {
        v[currPos] = queueVector[i].front ();
        queueVector[i].pop ();
        currPos++;
      }
    }

    /*
    v.clear();
    
    for(queue<int>& q : queueVector)
    {
      while (!q.empty())
      {
        v.push_back(q.front());
        q.pop();
      }
    }
    */
  }   
}
/************************************************************/
/************************************************************/

