/*
  Filename   : Timer.cc 
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Implement Timer Class 
  Description: A Timer class for measuring intervals of time.
*/   

/************************************************************/
// Macro guard to prevent multiple inclusions

#ifndef TIMER_H 
#define TIMER_H

/************************************************************/
// System includes

#include <string>
#include <iostream>
#include <ctime>
/************************************************************/
class Timer 
{
public:
    Timer (); 
    void start();
    void stop();
    void reset();
    double getElapsedMs() const;
private:
    clock_t m_startClock;
    clock_t m_stopClock;
};

#endif

/************************************************************/

