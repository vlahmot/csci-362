/*
  Filename   : Timer.cc 
  Author     : Tom Hlavaty
  Course     : CSCI 362-01
  Assignment : Radix Sort
  Description: Implementation of Timer class
*/   

/************************************************************/
// System includes

#include <iostream>
#include <ctime>
/************************************************************/
// Local includes

#include "TimerTest.h"

/************************************************************/
// Using declarations
using std::clock;
/************************************************************/
// Function prototypes/global vars/typedefs

/************************************************************/

// Constructor/Destructor
//   Cannot repeat default arguments in the implementation
Timer::Timer (): m_startClock (0), m_stopClock (0)
{
}

Timer::~Timer () {};

/************************************************************/

void
Timer::start() 
{
    m_startClock = clock(); 
}

void
Timer::stop() 
{
    m_stopClock = clock();
}

void
Timer::reset() 
{
  m_startClock = m_stopClock = 0;
}

double
Timer::getElapsedMs() const
{
    return static_cast<double> (m_stopClock - m_startClock)
    / CLOCKS_PER_SEC;
}
/************************************************************/
/************************************************************/

